#include <ESC.h>
#include <Servo.h>

#include <digitalWriteFast.h>
#include <EnableInterrupt.h>

#define SERIAL_PORT_SPEED 115200
#define RC_NUM_CHANNELS 2

#define RC_CH1 0
#define RC_CH2 1

#define RC_CH1_INPUT A2
#define RC_CH2_INPUT A5

#define OUTPUT_LEFT 11
#define OUTPUT_RIGHT 10

#define RC_CH1_MAX 1760
#define RC_CH1_OFF 1510
#define RC_CH1_MIN 1260

#define RC_CH2_MAX 1800
#define RC_CH2_OFF 1370
#define RC_CH2_MIN 950

#define OUTPUT_MAX 1200
#define OUTPUT_OFF 1100
#define OUTPUT_MIN 1000
#define OUTPUT_ARM 500

#define OUTPUT_RIGHT_REVERSE 3
#define OUTPUT_LEFT_REVERSE 2

uint16_t rc_values[RC_NUM_CHANNELS];
uint32_t rc_start[RC_NUM_CHANNELS];
volatile uint16_t rc_shared[RC_NUM_CHANNELS];

uint16_t output_value;
uint16_t output_value_left;
uint16_t output_value_right;

ESC esc_left(OUTPUT_LEFT, OUTPUT_MIN, OUTPUT_MAX, OUTPUT_ARM);
ESC esc_right(OUTPUT_RIGHT, OUTPUT_MIN, OUTPUT_MAX, OUTPUT_ARM);

void rc_read_values()
{
  noInterrupts();
  memcpy(rc_values, (const void *)rc_shared, sizeof(rc_shared));
  interrupts();
}

void calc_input(uint8_t channel, uint8_t input_pin)
{
  if (digitalReadFast(input_pin) == HIGH)
  {
    uint32_t current_time = micros();
    rc_start[channel] = current_time;
  }
  else
  {
    uint16_t rc_compare = (uint16_t)(micros() - rc_start[channel]);
    //rc_compare = constrain(rc_compare, RC_CH2_MIN, RC_CH2_MAX);
    rc_shared[channel] = rc_compare;
  }
}

void calc_output_ch2(uint16_t value)
{
  if (value >= RC_CH2_OFF)
  {
    output_value = map(value, RC_CH2_OFF, RC_CH2_MAX, OUTPUT_MIN, OUTPUT_MAX);
    digitalWrite(OUTPUT_LEFT_REVERSE, HIGH);
    digitalWrite(OUTPUT_RIGHT_REVERSE, HIGH);
    delay(5);
    Serial.print("Frente: ");
    Serial.print(output_value);
  }
  else
  {
    output_value = map(value, RC_CH2_OFF, RC_CH2_MIN, OUTPUT_MIN, OUTPUT_MAX);
    digitalWrite(OUTPUT_LEFT_REVERSE, LOW);
    digitalWrite(OUTPUT_RIGHT_REVERSE, LOW);
    delay(5);
    Serial.print("Tras  : ");
    Serial.print(output_value);
  }
  Serial.println();
}

void calc_ch1() { calc_input(RC_CH1, RC_CH1_INPUT); }
void calc_ch2() { calc_input(RC_CH2, RC_CH2_INPUT); }

void setup()
{
  Serial.begin(SERIAL_PORT_SPEED);

  pinModeFast(RC_CH1_INPUT, INPUT);
  pinModeFast(RC_CH2_INPUT, INPUT);
  pinMode(OUTPUT_LEFT_REVERSE, OUTPUT);
  pinMode(OUTPUT_RIGHT_REVERSE, OUTPUT);
  //pinModeFast(OUTPUT_CH1, OUTPUT);
  //pinModeFast(OUTPUT_CH2, OUTPUT)
  enableInterrupt(RC_CH1_INPUT, calc_ch1, CHANGE);
  enableInterrupt(RC_CH2_INPUT, calc_ch2, CHANGE);
  delay(2000);
  esc_left.arm();
  esc_right.arm();
}

void loop()
{
  rc_read_values();
  calc_output_ch2(rc_values[RC_CH2]);
  esc_left.speed(output_value);
  esc_right.speed(output_value);

  //Serial.print("CH1:"); Serial.print(rc_values[RC_CH1]); Serial.print("\t");
  //Serial.print("CH2:"); Serial.println(rc_values[RC_CH2]);

  // delay(200);
}
