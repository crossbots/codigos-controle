#define LOCK_1 2
#define LOCK_2 3

void setup() {
  // put your setup code here, to run once:
  pinMode(LOCK_1, OUTPUT);
  pinMode(LOCK_2, OUTPUT);
  digitalWrite(LOCK_1, LOW);
  digitalWrite(LOCK_2, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  /*
  delay(2000);
  digitalWrite(LOCK_1, HIGH);
  digitalWrite(LOCK_2, LOW);
  delay(2000);
  digitalWrite(LOCK_1, LOW);
  digitalWrite(LOCK_2, HIGH);
  delay(2000);
  digitalWrite(LOCK_1, HIGH);
  digitalWrite(LOCK_2, HIGH);
  delay(2000);
  digitalWrite(LOCK_1, LOW);
  digitalWrite(LOCK_2, LOW);
  */
  digitalWrite(LOCK_1, HIGH);
  delay(2000);
  digitalWrite(LOCK_1, LOW);
  delay(2000);
}
