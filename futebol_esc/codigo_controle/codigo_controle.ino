#include <ESC.h>
#include <Servo.h>

#include <digitalWriteFast.h>
#include <EnableInterrupt.h>

#define SERIAL_PORT_SPEED 115200
#define RC_NUM_CHANNELS 2

#define RC_CH1 0
#define RC_CH2 1

#define RC_CH1_INPUT A2
#define RC_CH2_INPUT A5

#define OUTPUT_LEFT 11
#define OUTPUT_RIGHT 10

#define RC_CH1_MAX 1756
#define RC_CH1_OFF 1510
#define RC_CH1_MIN 1260
#define RC_CH1_DEADZONE_RADIUS 15

#define RC_CH2_MAX 1800
#define RC_CH2_OFF 1370
#define RC_CH2_MIN 950
#define RC_CH2_DEADZONE_RADIUS 15

#define OUTPUT_MAX 1300
#define OUTPUT_MIN 1000
#define OUTPUT_RIGHT_OFF 1120
#define OUTPUT_LEFT_OFF 1110
#define OUTPUT_ARM OUTPUT_MIN

#define OUTPUT_RIGHT_REVERSE 3
#define OUTPUT_LEFT_REVERSE 2

uint16_t rc_values[RC_NUM_CHANNELS];
uint32_t rc_start[RC_NUM_CHANNELS];
volatile uint16_t rc_shared[RC_NUM_CHANNELS];

uint16_t output_value;
uint16_t output_value_left;
uint16_t output_value_right;
uint16_t output_reverse_right_status = LOW;
uint16_t output_reverse_left_status = LOW;

ESC esc_left(OUTPUT_LEFT, OUTPUT_MIN, OUTPUT_MAX, OUTPUT_ARM);
ESC esc_right(OUTPUT_RIGHT, OUTPUT_MIN, OUTPUT_MAX, OUTPUT_ARM);

void rc_read_values()
{
    noInterrupts();
    memcpy(rc_values, (const void *)rc_shared, sizeof(rc_shared));
    interrupts();
}

void rc_apply_deadzone(uint16_t *value_ch1, uint16_t *value_ch2)
{
    int32_t new_value_ch1 = (int32_t)(*value_ch1);
    int32_t new_value_ch2 = (int32_t)(*value_ch2);
    // Serial.println(new_value_ch1);
    if ((new_value_ch1 > RC_CH1_OFF && new_value_ch1 - RC_CH1_DEADZONE_RADIUS <= RC_CH1_OFF) || (new_value_ch1 < RC_CH1_OFF && new_value_ch1 + RC_CH1_DEADZONE_RADIUS >= RC_CH1_OFF))
    {
        //Serial.println(new_value_ch1 + RC_CH1_DEADZONE_RADIUS);
        //Serial.print(new_value);
        *value_ch1 = RC_CH1_OFF;
        //Serial.print(*value_ch1);
        // rc_values[RC_CH1] = RC_CH1_OFF;
    }
    //Serial.println(rc_values[RC_CH1]);
    if ((new_value_ch2 > RC_CH2_OFF && new_value_ch2 - RC_CH2_DEADZONE_RADIUS <= RC_CH2_OFF) || (new_value_ch2 < RC_CH2_OFF && new_value_ch2 + RC_CH2_DEADZONE_RADIUS >= RC_CH2_OFF))
    {
        *value_ch2 = RC_CH2_OFF;
        // rc_values[RC_CH2] = RC_CH2_OFF;
    }
    /*
    Serial.print(*value_ch1);
    Serial.print(" ");
    Serial.print(*value_ch2);
    Serial.println();*/
}

void calc_input(uint8_t channel, uint8_t input_pin)
{
    if (digitalReadFast(input_pin) == HIGH)
    {
        uint32_t current_time = micros();
        rc_start[channel] = current_time;
    }
    else
    {
        uint16_t rc_compare = (uint16_t)(micros() - rc_start[channel]);
        //rc_compare = constrain(rc_compare, RC_CH2_MIN, RC_CH2_MAX);
        rc_shared[channel] = rc_compare;
    }
}

void calc_output_ch2(uint16_t value)
{
    if (value >= RC_CH2_OFF)
    { //&& value < RC_CH2_MAX
        output_value = map(value, RC_CH2_OFF, RC_CH2_MAX, OUTPUT_MIN, OUTPUT_MAX);
        digitalWrite(OUTPUT_LEFT_REVERSE, HIGH);
        digitalWrite(OUTPUT_RIGHT_REVERSE, HIGH);
        delay(5);
        // Serial.print("Frente: ");
        // Serial.print(output_value);
    }
    else
    {
        output_value = map(value, RC_CH2_OFF, RC_CH2_MIN, OUTPUT_MIN, OUTPUT_MAX);
        digitalWrite(OUTPUT_LEFT_REVERSE, LOW);
        digitalWrite(OUTPUT_RIGHT_REVERSE, LOW);
        delay(5);
        // Serial.print("Tras  : ");
        // Serial.print(output_value);
    }
    // Serial.println();
}

void calc_ch1() { calc_input(RC_CH1, RC_CH1_INPUT); }
void calc_ch2() { calc_input(RC_CH2, RC_CH2_INPUT); }

void calc_output_tank(uint16_t ch1_value, uint16_t ch2_value)
{

    int16_t x = map(ch1_value, RC_CH1_MIN, RC_CH1_MAX, -100, 100);
    int16_t y = map(ch2_value, RC_CH2_MIN, RC_CH2_MAX, -100, 100);
    /*
    Serial.print(x);
    Serial.print(" ");
    Serial.print(y);
    Serial.println();*/

    int16_t v = (100 - abs(x)) * (y / 100) + y;
    int16_t w = (100 - abs(y)) * (x / 100) + x;

    int16_t r = (v + w) / 2;
    int16_t l = (v - w) / 2;
    /*
    Serial.print(r);
    Serial.print(" ");
    Serial.print(l);
    Serial.println();*/
    if (r > 0)
    {
        //Passa o valor normal normalizado para o total
        output_value_right = map(r, 0, 100, OUTPUT_RIGHT_OFF - 10, OUTPUT_MAX);
        if (r <= 10)
        {
            output_value_right = OUTPUT_MIN;
        }
        if (output_reverse_right_status == HIGH && r >= 10)
        {
            output_reverse_right_status = LOW;
            digitalWriteFast(OUTPUT_RIGHT_REVERSE, LOW);
            delay(5);
        }
    }
    else
    {
        //Inverte o valor
        output_value_right = map(r, 0, -100, OUTPUT_RIGHT_OFF - 10, OUTPUT_MAX);
        if (r >= -10)
        {
            output_value_right = OUTPUT_MIN;
        }
        if (output_reverse_right_status == LOW && r <= -10)
        {
            output_reverse_right_status = HIGH;
            digitalWriteFast(OUTPUT_RIGHT_REVERSE, HIGH);
            delay(5);
        }
    }
    if (l > 0)
    {

        //Passa o valor normal normalizado para o total
        output_value_left = map(l, 0, 100, OUTPUT_LEFT_OFF - 10, OUTPUT_MAX);
        if (l <= 10)
        {
            output_value_left = OUTPUT_MIN;
        }
        if (output_reverse_left_status == HIGH && l >= 10)
        {
            output_reverse_left_status = LOW;
            digitalWriteFast(OUTPUT_LEFT_REVERSE, LOW);
            delay(5);
            /*
            Serial.print("Frente: ");
            Serial.print(output_value_left);
            Serial.println();*/
        }
    }
    else
    {
        //Inverte o valor
        output_value_left = map(l, 0, -100, OUTPUT_LEFT_OFF - 10, OUTPUT_MAX);
        if (l >= -10)
        {
            output_value_left = OUTPUT_MIN;
        }
        if (output_reverse_left_status == LOW && l <= -10)
        {
            output_reverse_left_status = HIGH;
            digitalWriteFast(OUTPUT_LEFT_REVERSE, HIGH);
            delay(5);
            /*
            Serial.print("Tras  : ");
            Serial.print(output_value_left);
            Serial.println();*/
        }
    }
    /*
    if (output_reverse_left_status)
    {
        Serial.print("-");
    }
    Serial.print(output_value_left);
    Serial.print(" ");
    if (output_reverse_right_status)
    {
        Serial.print("-");
    }
    Serial.print(output_value_right);
    Serial.println();*/
}

void setup()
{
    Serial.begin(SERIAL_PORT_SPEED);

    pinModeFast(RC_CH1_INPUT, INPUT);
    pinModeFast(RC_CH2_INPUT, INPUT);
    pinModeFast(OUTPUT_LEFT_REVERSE, OUTPUT);
    pinModeFast(OUTPUT_RIGHT_REVERSE, OUTPUT);
    digitalWriteFast(OUTPUT_LEFT_REVERSE, LOW);
    digitalWriteFast(OUTPUT_RIGHT_REVERSE, LOW);
    output_reverse_right_status = LOW;
    output_reverse_left_status = LOW;
    enableInterrupt(RC_CH1_INPUT, calc_ch1, CHANGE);
    enableInterrupt(RC_CH2_INPUT, calc_ch2, CHANGE);
    delay(2000);
    esc_left.arm();
    esc_right.arm();
}

void loop()
{
    rc_read_values();
    rc_apply_deadzone(&rc_values[RC_CH1], &rc_values[RC_CH2]);
    // calc_output_ch2(rc_values[RC_CH2])
    calc_output_tank(rc_values[RC_CH1], rc_values[RC_CH2]);
    esc_left.speed(output_value_left);
    esc_right.speed(output_value_right);
    /*
    Serial.print("CH1:");
    Serial.print(rc_values[RC_CH1]);
    Serial.print("\t");
    Serial.print("CH2:");
    Serial.println(rc_values[RC_CH2]);*/
    /*
    Serial.print("Left :");
    Serial.print(output_value_left);
    Serial.print("\t");
    Serial.print("Right:");
    Serial.println(output_value_right);*/
    // delay(200);
}
