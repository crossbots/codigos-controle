byte entrada = A5;
byte saida = 12;

void setup() {
  // put your setup code here, to run once:
  pinMode(entrada, INPUT);
  pinMode(saida, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  int on = digitalRead(entrada);
  if (on) {
    Serial.println(1);
    digitalWrite(saida, HIGH);
  } else {
    digitalWrite(saida, LOW);
  }
}
